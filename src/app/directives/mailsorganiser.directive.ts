import {Directive, ElementRef, OnInit, Renderer2} from "@angular/core";

@Directive({
  selector: '[appMailOrganise]'
})
export class MailsorganiserDirective implements OnInit{
  constructor(private currentElement: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {

    let selfThis = this;

    let validEmails = []; //Массив валидных имейорв
    let incorrectEmails = []; //Массив невалидных имейлов

    //Выделяем весь дом элемента как переменную
    let currentModuleDom = this.currentElement.nativeElement;
    //Объявляем переменную для каждого элемента для удобства
    let inputEl = currentModuleDom.getElementsByTagName('input')[0];
    let emailsContainer = currentModuleDom.getElementsByClassName('EmailsEditor__content')[0];

    //Этлемент текущей строки в инпуте. будет менятся при изменении инпута
    let itemString = '';
    //Массив введённых имейлов
    let mailArr = [];

    //Форисированный перевод фокуса на инпут при клике на контейнер с уже добавленными итемами и самим инпутом
    emailsContainer.onclick = function () {
      inputEl.focus();
    };


    //Мониторинг ввода в инпут а так же запуск обработки введённого по нажатию
    inputEl.onkeyup = function (event) {
      if ((event.keyCode == 188) || (event.keyCode == 13)) {
        checkInputValue(this);
      }
    }

    //Событие по потере фокуса
    inputEl.onblur = function() {
      checkInputValue(this);
    };


    //Подсчёт имейлов
    document.getElementById('CountEmails').onclick = function() {
      finalEmailsArray();
    };

    //Обработка имейлов по кнопке
    document.getElementById('AddEmail').onclick = function() {
      mailArr = [];//Отчищаем массив элементов
      checkInputValue(this);
    };


    // проверка значений в инпуте
    function checkInputValue(el) {
      if (el.value.length > 2) {
        itemString = el.value;
        inputEl.value = "";
        organisingEmails(itemString);
      }
      else {
        // clear input
        inputEl.value = "";
      }
    }


    function organisingEmails(StringForOrganising){
      //Создаём массив из имейлов и попутно чистим их от пробелов(в имейлах не должно быть пробелов)
      mailArr = itemString.replace(/\s+/g,'').split(',');

      //Удаляем из массива пустые элементы
      mailArr.forEach(function(item, i, arr){
        if (item.length <1) {
          arr.splice(i, 1);
        }
      })
      //Пробегаемся циклом по массиву и создаём из каждого элемента блок с имейлом
      mailArr.forEach(function (item, i, arr) {

        const newEmail = selfThis.renderer.createElement('div'); //Создаём элемент для выделенного имейла
        const text = selfThis.renderer.createText(item); // Заполняем данными
        const className = selfThis.renderer.addClass(newEmail, 'MarkedEmail'); //Накидываем класс
        const deleteBtn = selfThis.renderer.createElement('i'); //Создаём кнопку удаления
        const deleteBtnClass = selfThis.renderer.addClass(deleteBtn, 'Icon'); //Накидываем класс
        const deleteBtnClass2 = selfThis.renderer.addClass(deleteBtn, 'Icon-delete'); //И ещё один

        //Удаление имейлов
        deleteBtn.onclick = function () {
          this.parentElement.remove();
        }

        selfThis.renderer.appendChild(newEmail, text); //Вставлям текст в выделенный блок
        selfThis.renderer.appendChild(newEmail, deleteBtn); //Вставляем кнопку удаления


        selfThis.renderer.insertBefore(emailsContainer, newEmail, inputEl); //Втыкаем получившийся блок перед инпутом

        //Проверка на валидность имейла. Простая, только на наличие собаки и точки. Более - излишне из-за возможных "специфичных" доменов.
        if (!newEmail.innerText.includes('@') || !newEmail.innerText.includes('.')) {
          selfThis.renderer.addClass(newEmail, 'MarkedEmail__incorret');
        }

      })
    }

    //Формирование итоговых массивов имейлов- валидных и не валидных.
    function finalEmailsArray() {
      let markedEmails = currentModuleDom.getElementsByClassName('MarkedEmail');

      incorrectEmails = [];
      validEmails = [];

      for (var i = 0; i < markedEmails.length; i++) {
        if (markedEmails[i].classList.contains('MarkedEmail__incorret')) {
          incorrectEmails.push(markedEmails[i].innerText)
        }
        else {
          validEmails.push(markedEmails[i].innerText)
        }
      }

      alert('Всего имейлов: '+ markedEmails.length + '\n' + "Из них корректных: " + validEmails.length + "\n" + "Некорректных: " + incorrectEmails.length);
    }
  }
}
