import { Component, OnInit, NgModule } from '@angular/core';
import {MailsorganiserDirective} from "../../directives/mailsorganiser.directive";


@NgModule({
  declarations: [
    MailsorganiserDirective
  ],
})

@Component({
  selector: 'app-emails-editor',
  templateUrl: './emails-editor.component.html',
  styleUrls: ['./emails-editor.component.sass']
})
export class EmailsEditorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
