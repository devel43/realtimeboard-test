import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { EmailsEditorComponent } from './components/emails-editor/emails-editor.component';
import { MailsorganiserDirective } from './directives/mailsorganiser.directive';


@NgModule({
  declarations: [
    AppComponent,
    EmailsEditorComponent,
    MailsorganiserDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
